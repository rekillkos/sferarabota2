@if($message = session()->get('message'))
    <div class="alert alert-success">
        {{ $message }}
    </div>
@endif
