<?php

namespace App\Traits;

use App\Dto\DTO;
use Illuminate\Database\Eloquent\Builder;

trait Dtoable
{
    public function scopeCollection(Builder $query, DTO $dto)
    {
        $query->sort($dto->sort)->filters($dto->filters);
        if (method_exists($this, 'scopeSearch')) {
            $query->search($dto->search);
        }
        if ($dto->perPage > 0) {
            $result = $query->paginate($dto->perPage, ["*"], 'page', $dto->page);
        } else {
            $result = $query->get();
        }
        $result->appends($dto->paginationAppends);
        return $result;
    }
}
