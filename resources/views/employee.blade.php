@extends('layouts/app')

@section('content')
    <dl class="dl-horizontal">
        <dt>ФИО</dt>
        <dd>
            @auth
                {{ $employee->fio }}
            @endauth
        </dd>
    </dl>
    <dl class="dl-horizontal">
        <dt>Телефон</dt>
        <dd>
            @auth
                {{ $employee->phone }}
            @endauth
        </dd>
    </dl>
    <dl class="dl-horizontal">
        <dt>Email</dt>
        <dd>
            @auth
                {{ $employee->email }}
            @endauth
        </dd>
    </dl>
    <dl class="dl-horizontal">
        <dt>Должность</dt>
        <dd>{{ $employee->position->name ?? '' }}</dd>
    </dl>
    <dl class="dl-horizontal">
        <dt>Навыки</dt>
        <dd>
            <ul>
                @foreach ($employee->skills as $skill)
                    <li>{{ $skill->name }}</li>
                @endforeach
            </ul>
        </dd>
    </dl>
    <a href="{{ route('home') }}" class="btn btn-primary">Назад</a>
@endsection
