<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Str;

class RegisterUserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegister()
    {
        $userData = [
            'fio' => Str::random(10),
            'email' => Str::random(5) . '@' . Str::random(5) . '.' . Str::random(2),
            'phone' => '123123123',
            'password' => '123123123',
            'password_confirmation' => '123123123',
        ];
        $response = $this->post(route('register'), $userData);
        $response->assertRedirect(route('home'));
        $this->assertAuthenticated();
    }
}
