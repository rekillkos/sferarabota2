@extends('admin/admin')

@section('columncontent')
    <div class="row">
        <div class="col-md-2">
            <a href="{{ route('admin.position.create') }}" class="btn btn-primary">Создать</a>
        </div>
    </div>
    <br>
    @include('components/message')
    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>Наименование</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($positions as $position)
                <tr>
                    <td>{{ $position->name }}</td>
                    <td>
                        <a href="{{ route('admin.position.edit', ['position' => $position]) }}" class="btn btn-primary">Редактировать</a>
                        <button class="btn btn-danger" onclick="deleteModel('position', {{ $position->id }})">Удалить</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $positions->links() ?? '' }}
@endsection
