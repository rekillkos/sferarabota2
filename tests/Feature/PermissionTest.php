<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class PermissionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testPermission()
    {
        $response = $this->post(route('login'), [
            'email' => 'user@user.user',
            'password' => '123123123',
        ]);
        $response->assertRedirect();
        $user = User::where('email', 'user@user.user')->first();
        $this->assertAuthenticatedAs($user);
        $response = $this->get(route('admin.dashboard'));
        $response->assertStatus(403);
    }
}
