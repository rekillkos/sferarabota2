@extends('admin/admin')

@section('columncontent')
    @include('components/error')
    <form action="{{ isset($skill) ? route('admin.skill.update', ['skill' => $skill]) : route('admin.skill.store') }}" method="post">
        @csrf
        @method(isset($skill) ? 'put' : 'post')
        <div class="form-group">
            <label for="name">Наименование</label>
            <input name="name" type="text" class="form-control" value="{{ old('name') ?? $skill->name ?? '' }}" autofocus required>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
