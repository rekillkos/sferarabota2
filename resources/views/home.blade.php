@extends('layouts/app')

@section('content')
    <form action="" method="get">
        <div class="row">
            <div class="col-md-2">
                <label for="sort">Сортировка</label>
                <select name="sort" id="sort" class="form-control">
                    <option value="alphabet" @if (request()->get('sort') == 'alphabet') selected @endif>По алфавиту</option>
                    <option value="reg_date" @if (request()->get('sort') == 'reg_date') selected @endif>По дате регистрации</option>
                </select>
            </div>
            <div class="col-md-2">
                <label for="filter_position">Должность</label>
                <select name="filter[position]" id="filter_position" class="form-control">
                    <option value=""></option>
                    @foreach ($positions as $positionId => $positionName)
                        <option value="{{ $positionId }}" @if (request()->input('filter.position') == $positionId) selected @endif>{{ $positionName }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label for="filter_skill">Навык</label>
                <select name="filter[skill]" id="filter_skill" class="form-control">
                    <option value=""></option>
                    @foreach ($skills as $skillId => $skillName)
                        <option value="{{ $skillId }}" @if (request()->input('filter.skill') == $skillId) selected @endif>{{ $skillName }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label for="search">Поиск</label>
                <input type="text" name="search" id="search" value="{{ request()->input('search') }}" class="form-control" placeholder="ФИО, Email, должность, навык">
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">Отправить</button>
            </div>
        </div>
    </form>
    <br>
    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>ФИО</th>
                <th>Email</th>
                <th>Телефон</th>
                <th>Должность</th>
                <th>Навыки</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
                <tr>
                    <td>
                        @auth
                            {{ $employee->fio }}
                        @endauth
                    </td>
                    <td>
                        @auth
                            {{ $employee->email }}
                        @endauth
                    </td>
                    <td>
                        @auth
                            {{ $employee->phone }}
                        @endauth
                    </td>
                    <td>{{ $employee->position->name ?? '' }}</td>
                    <td>
                        <ul>
                            @foreach ($employee->skills as $skill)
                                <li>{{ $skill->name }}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <a href="{{ route('employee', ['employee' => $employee]) }}" class="btn btn-primary">Карточка</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $employees->links() ?? '' }}
@endsection
