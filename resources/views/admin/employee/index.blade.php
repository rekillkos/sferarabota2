@extends('admin/admin')

@section('columncontent')
    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>ФИО</th>
                <th>Email</th>
                <th>Телефон</th>
                <th>Должность</th>
                <th>Навыки</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
                <tr>
                    <td>{{ $employee->fio }}</td>
                    <td>{{ $employee->email }}</td>
                    <td>{{ $employee->phone }}</td>
                    <td>{{ $employee->position->name ?? '' }}</td>
                    <td>
                        <ul>
                            @foreach ($employee->skills as $skill)
                                <li>{{ $skill->name }}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <a href="{{ route('admin.employee.edit', ['employee' => $employee]) }}" class="btn btn-primary">Редактировать</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $employees->links() ?? '' }}
@endsection
