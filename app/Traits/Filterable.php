<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * @method Builder filters(array $filters)
 * @method Builder filter(string $field, $value)
 */
trait Filterable
{
    public function scopeFilters(Builder $query, array $filters)
    {
        foreach ($filters as $key => $filter) {
            if (!isset($filter['field'])) {
                $query->filter($key, $filter);
            } else {
                $query->filter(
                    $filter['field'],
                    $filter['value']
                );
            }
        }
    }

    public function scopeFilter(Builder $query, string $field, $value)
    {
        if (!$value) {
            return;
        }
        $tableName = static::getTableName();
        $filterMethodName = 'filter' . Str::studly($field) . 'Attribute';
        if (method_exists($this, $filterMethodName)) {
            $this->$filterMethodName($query, $value);
            return;
        }
        if (is_array($value)) {
            $query->whereIn("${tableName}.${field}", $value);
            return;
        }
        $query->where("${tableName}.${field}", $value);
    }
}
