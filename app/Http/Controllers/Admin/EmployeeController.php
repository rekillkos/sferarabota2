<?php

namespace App\Http\Controllers\Admin;

use App\Dto\ModelDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\Position;
use App\Models\Skill;
use App\Services\EmployeeService;
use Spatie\Permission\Models\Role;

class EmployeeController extends Controller
{
    private EmployeeService $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function index(ModelDTO $dto)
    {
        $employees = Employee::query()
            ->with(['position', 'skills'])
            ->collection($dto);
        $templateData = [
            'employees' => $employees,
        ];
        return view('admin/employee/index', $templateData);
    }

    public function edit(Employee $employee)
    {
        $employee->setAppends(['skill_ids']);
        $positions = Position::pluck('name', 'id');
        $skills = Skill::pluck('name', 'id');
        $roles = Role::pluck('name', 'id');
        $templateData = [
            'employee' => $employee,
            'positions' => $positions,
            'skills' => $skills,
            'roles' => $roles,
        ];
        return view('admin/employee/form', $templateData);
    }

    public function update(Employee $employee, EmployeeRequest $request)
    {
        $this->employeeService->update($employee, $request->all());
        return redirect()->route('admin.employee.index')->withMessage('Обновлено');
    }
}
