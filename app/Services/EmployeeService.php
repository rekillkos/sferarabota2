<?php

namespace App\Services;

use App\Models\Employee;

class EmployeeService
{
    /**
     * Обновление сотрудника
     *
     * @param Employee $employee
     * @param array $data
     * @return Employee
     */
    public function update(Employee $employee, array $data): Employee
    {
        $employee->update($data);
        if (isset($data['skill_ids'])) {
            $employee->skills()->sync($data['skill_ids']);
        }
        if (isset($data['role_id'])) {
            $employee->user->syncRoles($data['role_id']);
        }
        return $employee->refresh();
    }
}
