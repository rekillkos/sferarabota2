<?php

namespace App\Models;

use App\Traits\Dtoable;
use App\Traits\Filterable;
use App\Traits\Sortable;

/**
 * Навык
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 */
class Skill extends BaseModel
{
    use Dtoable,
        Sortable,
        Filterable;

    protected $table = 'skills';

    protected $fillable = [
        'name',
    ];
}
