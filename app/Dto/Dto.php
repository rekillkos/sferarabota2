<?php

namespace App\Dto;

class DTO
{
    public array $filters = [];

    public array $paginationAppends = [];

    public string $sort = '';

    public string $search = '';

    public int $page = 0;

    public int $perPage = 0;

    public function addFilters(array $filters)
    {
        $this->filters = array_merge($this->filters, $filters);
    }

    public function setSort(string $sort)
    {
        $this->sort = $sort;
    }

    public function setSearch(string $search)
    {
        $this->search = $search;
    }

    public function setPagination(int $page, int $perPage, array $paginationAppends)
    {
        $this->page = $page;
        $this->perPage = $perPage;
        $this->paginationAppends = $paginationAppends;
    }
}
