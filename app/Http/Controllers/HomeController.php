<?php

namespace App\Http\Controllers;

use App\Dto\ModelDTO;
use App\Models\Employee;
use App\Models\Position;
use App\Models\Skill;

class HomeController extends Controller
{
    public function index(ModelDTO $dto)
    {
        $employees = Employee::query()
            ->with(['position', 'skills'])
            ->filter('doesntHaveRole', 'admin')
            ->collection($dto);
        $positions = Position::pluck('name', 'id');
        $skills = Skill::pluck('name', 'id');
        $templateData = [
            'employees' => $employees,
            'positions' => $positions,
            'skills' => $skills,
        ];
        return view('home', $templateData);
    }
}
