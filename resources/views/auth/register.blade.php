@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8">
            @include('components/error')
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group">
                    <label for="fio">ФИО</label>
                    <input id="fio" type="text" class="form-control" name="fio" value="{{ old('fio') }}" required autocomplete="fio" autofocus>
                </div>
                <div class="form-group">
                    <label for="phone">Телефон</label>
                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autocomplete="phone">
                </div>
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email">
                </div>
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">
                </div>
                <div class="form-group">
                    <label for="password-confirm">Подтверждение пароля</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Регистрация</button>
                </div>
            </form>
        </div>
    </div>
@endsection
