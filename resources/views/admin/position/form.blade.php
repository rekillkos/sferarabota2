@extends('admin/admin')

@section('columncontent')
    @include('components/error')
    <form action="{{ isset($position) ? route('admin.position.update', ['position' => $position]) : route('admin.position.store') }}" method="post">
        @csrf
        @method(isset($position) ? 'put' : 'post')
        <div class="form-group">
            <label for="name">Наименование</label>
            <input name="name" type="text" class="form-control" value="{{ old('name') ?? $position->name ?? '' }}" autofocus required>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
