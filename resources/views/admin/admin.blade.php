@extends('layouts/app')

@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li @if (request()->is('admin/employee*')) class="active" @endif>
                            <a href="{{ route('admin.employee.index') }}">Сотрудники</a>
                        </li>
                        <li @if (request()->is('admin/position*')) class="active" @endif>
                            <a href="{{ route('admin.position.index') }}">Должности</a>
                        </li>
                        <li @if (request()->is('admin/skill*')) class="active" @endif>
                            <a href="{{ route('admin.skill.index') }}">Навыки</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            @yield('columncontent')
        </div>
    </div>
@endsection
