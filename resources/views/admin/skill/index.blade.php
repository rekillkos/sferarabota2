@extends('admin/admin')

@section('columncontent')
    <div class="row">
        <div class="col-md-2">
            <a href="{{ route('admin.skill.create') }}" class="btn btn-primary">Создать</a>
        </div>
    </div>
    <br>
    @include('components/message')
    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>Наименование</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($skills as $skill)
                <tr>
                    <td>{{ $skill->name }}</td>
                    <td>
                        <a href="{{ route('admin.skill.edit', ['skill' => $skill]) }}" class="btn btn-primary">Редактировать</a>
                        <button class="btn btn-danger" onclick="deleteModel('skill', {{ $skill->id }})">Удалить</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $skills->links() ?? '' }}
@endsection
