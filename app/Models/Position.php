<?php

namespace App\Models;

use App\Traits\Dtoable;
use App\Traits\Filterable;
use App\Traits\Sortable;

/**
 * Должность
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $name
 */
class Position extends BaseModel
{
    use Dtoable,
        Sortable,
        Filterable;

    protected $table = 'positions';

    protected $fillable = [
        'name',
    ];
}
