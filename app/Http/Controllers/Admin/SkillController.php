<?php

namespace App\Http\Controllers\Admin;

use App\Dto\ModelDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\SkillRequest;
use App\Models\Skill;
use App\Services\SkillService;

class SkillController extends Controller
{
    private SkillService $skillService;

    public function __construct(SkillService $skillService)
    {
        $this->skillService = $skillService;
    }

    public function index(ModelDTO $dto)
    {
        $skills = Skill::query()->collection($dto);
        $templateData = [
            'skills' => $skills
        ];
        return view('admin/skill/index', $templateData);
    }

    public function create()
    {
        return view('admin/skill/form');
    }

    public function store(SkillRequest $request)
    {
        $this->skillService->create($request->only('name'));
        return redirect()->route('admin.skill.index')->withMessage('Создано');
    }

    public function edit(Skill $skill)
    {
        $templateData = [
            'skill' => $skill,
        ];
        return view('admin/skill/form', $templateData);
    }

    public function update(Skill $skill, SkillRequest $request)
    {
        $this->skillService->update($skill, $request->only('name'));
        return redirect()->route('admin.skill.index')->withMessage('Обновлено');
    }

    public function destroy(Skill $skill)
    {
        $this->skillService->remove($skill);
        return redirect()->route('admin.skill.index')->withMessage('Удалено');
    }
}
