<?php

namespace App\Traits\Filter;

use Illuminate\Database\Eloquent\Builder;

trait EmployeeFilter
{
    public function filterPositionAttribute(Builder $query, $value)
    {
        $query->where('position_id', $value);
    }

    public function filterSkillAttribute(Builder $query, $value)
    {
        $query->whereHas('skills', fn ($q) => $q->where('id', $value));
    }

    public function filterDoesntHaveRoleAttribute(Builder $query, $roleName)
    {
        $query->whereDoesntHave('user', fn ($userQuery) =>
            $userQuery->whereDoesntHave('roles', fn ($rolesQuery) =>
                $rolesQuery->where('name', '!=', $roleName)));
    }
}
