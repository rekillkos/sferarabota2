@extends('admin/admin')

@section('columncontent')
    @include('components/error')
    <form action="{{ route('admin.employee.update', ['employee' => $employee]) }}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="fio">ФИО</label>
            <input name="fio" type="text" class="form-control" value="{{ old('fio') ?? $employee->fio ?? '' }}" autofocus required>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input name="email" type="text" class="form-control" value="{{ old('email') ?? $employee->email ?? '' }}" autofocus required>
        </div>
        <div class="form-group">
            <label for="phone">Телефон</label>
            <input name="phone" type="text" class="form-control" value="{{ old('phone') ?? $employee->phone ?? '' }}" autofocus required>
        </div>
        <div class="form-group">
            <label for="position">Должность</label>
            <select name="position_id" id="position" class="form-control">
                @foreach ($positions as $positionId => $positionName)
                    <option value="{{ $positionId }}" @if ($positionId == (old('position_id') ?? $employee->position->id ?? null)) selected @endif>{{ $positionName }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="skill">Навыки</label>
            <div class="row">
                @foreach ($skills as $skillId => $skillName)
                    <div class="col-md-2">
                        <input type="checkbox" name="skill_ids[]" value="{{ $skillId }}" @if (in_array($skillId, (old('skill_ids') ?? $employee->skill_ids))) checked @endif> {{ $skillName }}
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <label for="role">Роль</label>
            <select name="role_id" id="role" class="form-control">
                @foreach ($roles as $roleId => $roleName)
                    <option value="{{ $roleId }}" @if (old('role_id') ? $roleId == old('role_id') : $employee->user->hasRole($roleName)) selected @endif>{{ $roleName }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
