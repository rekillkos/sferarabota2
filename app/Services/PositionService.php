<?php

namespace App\Services;

use App\Models\Position;

class PositionService
{
    /**
     * Создание должности
     *
     * @param array $data
     * @return Position
     */
    public function create(array $data): Position
    {
        return Position::create($data);
    }

    /**
     * Обновление должности
     *
     * @param Position $position
     * @param array $data
     * @return Position
     */
    public function update(Position $position, array $data): Position
    {
        $position->update($data);
        return $position->refresh();
    }

    /**
     * Удаление должности
     *
     * @param Position $position
     * @return void
     */
    public function remove(Position $position)
    {
        $position->delete();
    }
}
