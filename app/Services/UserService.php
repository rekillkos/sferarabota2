<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * Создание пользователя
     *
     * @param array $data
     * @return User
     */
    public function create(array $data): User
    {
        $user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        $user->assignRole('user');
        $user->employee()->updateOrCreate([
            'fio' => $data['fio'],
            'phone' => $data['phone'],
        ], [
            'email' => $data['email'],
        ]);
        return $user;
    }
}
