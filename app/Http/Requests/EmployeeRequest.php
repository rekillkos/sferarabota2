<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fio' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'skill_ids' => ['array', 'max:5'],
        ];
    }

    public function attributes()
    {
        return [
            'fio' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'Email',
            'skill_ids' => 'Навыки',
        ];
    }
}
