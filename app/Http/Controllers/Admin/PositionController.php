<?php

namespace App\Http\Controllers\Admin;

use App\Dto\ModelDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\PositionRequest;
use App\Models\Position;
use App\Services\PositionService;

class PositionController extends Controller
{
    private PositionService $positionService;

    public function __construct(PositionService $positionService)
    {
        $this->positionService = $positionService;
    }

    public function index(ModelDTO $dto)
    {
        $positions = Position::query()->collection($dto);
        $templateData = [
            'positions' => $positions
        ];
        return view('admin/position/index', $templateData);
    }

    public function create()
    {
        return view('admin/position/form');
    }

    public function store(PositionRequest $request)
    {
        $this->positionService->create($request->only('name'));
        return redirect()->route('admin.position.index')->withMessage('Создано');
    }

    public function edit(Position $position)
    {
        $templateData = [
            'position' => $position,
        ];
        return view('admin/position/form', $templateData);
    }

    public function update(Position $position, PositionRequest $request)
    {
        $this->positionService->update($position, $request->only('name'));
        return redirect()->route('admin.position.index')->withMessage('Обновлено');
    }

    public function destroy(Position $position)
    {
        $this->positionService->remove($position);
        return redirect()->route('admin.position.index')->withMessage('Удалено');
    }
}
