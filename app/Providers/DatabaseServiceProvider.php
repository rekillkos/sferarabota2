<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        DB::connection()->getPdo()->sqliteCreateFunction('sqlite_lower', fn ($string) => mb_strtolower($string), 1);
    }
}
