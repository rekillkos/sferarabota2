<?php

namespace App\Services;

use App\Models\Skill;

class SkillService
{
    /**
     * Создание навыка
     *
     * @param array $data
     * @return Skill
     */
    public function create(array $data): Skill
    {
        return Skill::create($data);
    }

    /**
     * Обновление навыка
     *
     * @param Skill $skill
     * @param array $data
     * @return Skill
     */
    public function update(Skill $skill, array $data): Skill
    {
        $skill->update($data);
        return $skill->refresh();
    }

    /**
     * Удаление навыка
     *
     * @param Skill $skill
     * @return void
     */
    public function remove(Skill $skill)
    {
        $skill->delete();
    }
}
