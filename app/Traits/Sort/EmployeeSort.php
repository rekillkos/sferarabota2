<?php

namespace App\Traits\Sort;

use Illuminate\Database\Eloquent\Builder;

trait EmployeeSort
{
    public function sortAlphabetAttribute(Builder $query)
    {
        $query->orderBy('fio', 'asc');
    }

    public function sortRegDateAttribute(Builder $query)
    {
        $query->orderBy('created_at', 'asc');
    }
}
