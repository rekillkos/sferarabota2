<?php

namespace App\Http\Controllers;

use App\Models\Employee;

class EmployeeController extends Controller
{
    public function show(Employee $employee)
    {
        $templateData = [
            'employee' => $employee,
        ];
        return view('employee', $templateData);
    }
}
