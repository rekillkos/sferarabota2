function deleteModel(type, id) {
    var csrf = jQuery('meta[name="csrf-token"]').attr('content');
    var form = jQuery(`<form method="post" action="admin/${type}/${id}">` +
        `<input type="hidden" name="_token" value="${csrf}">` +
        `<input type="hidden" name="_method" value="delete">` +
    `</form>`);
    jQuery('body').append(form);
    form.trigger('submit');
}
