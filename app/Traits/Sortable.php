<?php

namespace App\Traits;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * @method Builder sort(Builder $query, array $sort)
 */
trait Sortable
{
    public function scopeSort(Builder $query, string $sort)
    {
        if (!$sort) {
            return;
        }
        $tableName = $this->getTable();
        $appendFields = $this->getArrayableAppends();
        $sortMethodName = 'sort' . Str::studly($sort) . 'Attribute';
        if (method_exists($this, $sortMethodName)) {
            $this->$sortMethodName($query);
            return;
        }
        if (!in_array($sort, $appendFields)) {
            $query->orderBy("${tableName}.${sort}", "asc");
            return;
        }
        throw new Exception("Невозможно отсортировать по полю '${sort}'");
    }
}
