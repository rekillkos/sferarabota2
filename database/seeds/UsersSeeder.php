<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrCreate([
            'email' => 'admin@admin.admin',
        ], [
            'password' => Hash::make('123123123'),
        ]);
        $user->assignRole('admin');

        $user = User::firstOrCreate([
            'email' => 'user@user.user',
        ], [
            'password' => Hash::make('123123123'),
        ]);
        $user->assignRole('user');
    }
}
