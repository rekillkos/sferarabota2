<?php

namespace App\Models;

use App\Traits\Dtoable;
use App\Traits\Filter\EmployeeFilter;
use App\Traits\Sortable;
use App\Traits\Filterable;
use App\Traits\Search\EmployeeSearch;
use App\Traits\Sort\EmployeeSort;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Сотрудник
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $update_at
 * @property int $user_id
 * @property string $fio
 * @property string $email
 * @property string $phone
 * @property int $positon_id
 */
class Employee extends BaseModel
{
    use Dtoable,
        Sortable,
        Filterable,
        EmployeeSort,
        EmployeeFilter,
        EmployeeSearch;

    protected $table = 'employees';

    protected $fillable = [
        'fio',
        'email',
        'phone',
        'position_id',
    ];

    /**
     * ID навыков
     *
     * @return array
     */
    public function getSkillIdsAttribute(): array
    {
        return $this->skills()->pluck('id')->toArray();
    }

    /**
     * Должность
     *
     * @return BelongsTo
     */
    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class);
    }

    /**
     * Навыки
     *
     * @return BelongsToMany
     */
    public function skills(): BelongsToMany
    {
        return $this->belongsToMany(Skill::class);
    }

    /**
     * Пользователь
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
