<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class LoginUserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->post(route('login'), [
            'email' => 'admin@admin.admin',
            'password' => '123123123',
        ]);
        $response->assertRedirect();
        $user = User::where('email', 'admin@admin.admin')->first();
        $this->assertAuthenticatedAs($user);
    }
}
