<?php

namespace App\Traits\Search;

use App\Models\Employee;
use App\Models\Position;
use App\Models\Skill;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * @method Builder search(Builder $query, string $search)
 */
trait EmployeeSearch
{
    public function scopeSearch(Builder $query, string $search)
    {
        if (!$search) {
            return;
        }
        $search = mb_strtolower($search);
        $query->where(function ($q) use ($search) {
            $employeeTableName = Employee::getTableName();
            $positionTableName = Position::getTableName();
            $skillTableName = Skill::getTableName();
            $q->where(DB::raw("sqlite_lower({$employeeTableName}.fio)"), "like", "%{$search}%")
                ->orWhere(DB::raw("sqlite_lower({$employeeTableName}.email)"), "like", "%{$search}%")
                ->orWhereHas("position", fn ($p) => $p->where(DB::raw("sqlite_lower({$positionTableName}.name)"), "like", "%{$search}%"))
                ->orWhereHas("skills", fn ($s) => $s->where(DB::raw("sqlite_lower({$skillTableName}.name)"), "like", "%{$search}%"));
        });
    }
}
