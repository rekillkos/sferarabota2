<?php

namespace App\Dto;

use Illuminate\Http\Request;

class ModelDTO extends DTO
{
    const PER_PAGE = 20;

    public function __construct(Request $request)
    {
        $this->addFilters($request->input('filter') ?? []);
        $this->setSort($request->input('sort') ?? '');
        $this->setSearch($request->input('search') ?? '');
        $this->setPagination(intval($request->input('page', 0)), self::PER_PAGE, $request->only(['filter', 'sort', 'search']));
    }
}
